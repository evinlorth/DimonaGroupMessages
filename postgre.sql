CREATE TABLE IF NOT EXISTS "messages" (
  "id" serial NOT NULL,
  PRIMARY KEY ("id"),
  "date" timestamp NOT NULL,
  "message" text NOT NULL
);

CREATE TABLE IF NOT EXISTS "user" (
    "id" serial NOT NULL,
    PRIMARY KEY ("id"),
    "username" VARCHAR(16) NOT NULL,
    "password" VARCHAR(16) NOT NULL
);