import re


def review_text(text):
    text = set_bold(text)
    text = set_link(text)
    return text


def set_link(text):
    revised_text = ""
    pattern = r"(^.*)(http[:\/\$\-\_\.\+\!\*\'\(\)\%\w]*)(\s{0}.*)"
    for line in text.split("\n"):
        matches = re.findall(pattern, line)
        if matches:
            for match in matches[0]:
                if match.startswith("http"):
                    revised_text += f"<a target='_blank' href='{match}'>{match}</a>"
                else:
                    revised_text += match
        else:
            revised_text += line
        revised_text += "\n"
    return revised_text


def set_bold(text):
    revised_text = ""

    for line in text.split("\n"):
        revised_text += check_line(line, revised_text) + "\n"
    return revised_text


def check_line(text, revised_text):
    pattern = r"(^[^\*]*)(\*[\*|\w][^\*]*\*)(.*)"
    matches = re.findall(pattern, text)
    if matches:
        first_line = True
        for match in matches[0]:
            if match.startswith("*") and match.endswith("*"):
                revised_text += f"<b>{match[1: -1]}</b>"
            else:
                if first_line:
                    revised_text += match
                    first_line = False
                else:
                    revised_text += check_line(match, revised_text)
        return revised_text
    else:
        return text
