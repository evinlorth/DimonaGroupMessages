import functools
import os

from flask import Flask, render_template, request, url_for, redirect
from markupsafe import Markup

from helpers.whatsapp import review_text
from model.messages import Messages
from model.user import User

app = Flask(__name__)

current_user = None


def login_required(func):
    @functools.wraps(func)
    def wrapper():
        if current_user is not None:
            return func()
        return render_template('login.html')
    return wrapper


@app.route("/messages")
def show_messages():
    messages = Messages().get_all()
    for msg in messages:
        msg["message"] = Markup(msg["message"].replace("\n", "<br />"))

    return render_template('show_messages.html', messages=messages)


@app.route("/add_message", methods=["GET"])
@login_required
def add_message_page():
    return render_template('translated_message_page.html')


@app.route("/save_inserted_message", methods=["POST"])
@login_required
def save_message():
    message = request.form["message"]
    message = review_text(message)
    if Messages().save(message):
        return {"status": True}
    return {"status": False}


@app.route('/login', methods=["GET", "POST"])
def login():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        user = User().get_user_id(username=username, password=password)

        if user:
            login_user(user)
            return redirect(url_for('add_message_page'))

    return render_template('login.html')


@app.route('/error')
def error():
    return render_template('error.html')


@app.route('/logout')
@login_required
def logout():
    logout_user()
    return "Logged out successfully."


def login_user(user):
    global current_user
    current_user = user


def logout_user():
    global current_user
    current_user = None


if __name__ == '__main__':
    debug = os.getenv("DEBUG", None)
    app.run(host="0.0.0.0", port=os.environ["PORT"], debug=bool(debug))
