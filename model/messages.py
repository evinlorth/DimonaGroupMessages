import datetime
from functools import cache

from model import DB


class Messages(DB):
    @cache
    def get_all(self):
        self.cur.execute("SELECT * FROM messages ORDER BY date DESC")
        records = self.cur.fetchall()
        return [dict(row) for row in records]

    def save(self, message):
        params = {
            "date": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            "message": message
        }
        query = """
            INSERT INTO messages
            (date, message)
            VALUES 
            (%(date)s, %(message)s)
            RETURNING id
        """

        self.cur.execute(query, params)
        self.conn.commit()
        record = self.cur.fetchone()
        if record:
            return dict(record)["id"]
