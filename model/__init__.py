import os

import psycopg2
from psycopg2.extras import RealDictCursor


class DB:
    def __init__(self):
        self.conn = psycopg2.connect(f"dbname=dimona_group_messages user={os.environ['DBUSER']}")
        self.cur = self.conn.cursor(cursor_factory=RealDictCursor)
