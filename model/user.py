from flask_login import UserMixin

from model import DB


class User(DB, UserMixin):
    def get_user_id(self, username, password):
        params = {
            "username": username,
            "password": password,
        }
        self.cur.execute("SELECT id FROM public.user WHERE username = %(username)s AND  password = %(password)s", params)
        record = self.cur.fetchone()
        if record:
            return int(record["id"])

    def get_by_id(self, user_id):
        self.cur.execute("SELECT * FROM public.user WHERE id = %(user_id)s", {"user_id": user_id})
        record = self.cur.fetchone()
        return dict(record)
